$(window).on('load',function(){
    $('.preloader').addClass('done');
});

$(document).ready(function(){
    new WOW().init();

    $('.callBack input.phone').focusin(function(){  
        $(this).siblings('span.hint').addClass('active');
    }).focusout(function(){  
        $(this).siblings('span.hint').removeClass('active');  
    });

    $(document).on('change keypress', '.callBack input', function(){
        $(this).parents('label').removeClass('error');
        $(this).next('span.error').html('');
    });

    //form validation
    $(document).on('click','.callBack .bottom button', function(e){
        e.preventDefault();

        var validName = false,
            validPhone = false,
            validEmail = false;

        var name = $('.callBack input.name').val(),
            phone = $('.callBack input.phone').val(),
            email = $('.callBack input.email').val();

        //check name field
        if (!name){
            $('input.name').parents('label').addClass('error');
            $('input.name').next('span.error').html('Введите имя');
        } else if (name.length < 5){
            $('input.name').parents('label').addClass('error');
            $('input.name').next('span.error').html('Не менее 5 символов');
        } else if (!name.match(/^[А-Яа-яёЁЇїa-zA-Z\-\s]+$/i)){
            $('input.name').parents('label').addClass('error');
            $('input.name').next('span.error').html('Разрешены символы и пробелы');
        } else {
            validName = true;
        }

        //check phone field
        if (!phone){
            $('input.phone').parents('label').addClass('error');
            $('input.phone').next('span.error').html('Введите телефон');
        } else if (!phone.match(/^[\+]?[38]{2}\s?[(]?[0-9]{3}[)]?[\s]?[0-9]{3}[-\s]?[0-9]{2}[-\s]?[0-9]{2}$/i)){
            $('input.phone').parents('label').addClass('error');
            $('input.phone').next('span.error').html('Неверный формат телефона');
        } else {
            validPhone = true;
        }

        //check email field
        if (!email){
            $('input.email').parents('label').addClass('error');
            $('input.email').next('span.error').html('Введите email');
        } else if (!email.match(/^[0-9a-z_\.]+@[0-9a-z_^.]+.[a-z]{2,4}$/i)){
            $('input.email').parents('label').addClass('error');
            $('input.email').next('span.error').html('Неверный формат email');
        } else {
            validEmail = true;
        }

        if (validName && validPhone && validEmail){
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/callback',
                data: {
                    'name': name,
                    'phone': phone,
                    'email': email
                },
                success: function (response) {
                    $(".result.form-thanks").addClass('visible');
                    $('.wrapper').addClass('modal-show');
                    setTimeout(function() {
                        $(".result.form-thanks").removeClass('visible');
                        $('.wrapper').removeClass('modal-show');
                    }, 3000);
                },
                error: function (xhr, status, err) {
                    $(".result.form-error").addClass('visible');
                    $('.wrapper').addClass('modal-show');
                    setTimeout(function() {
                        $(".result.form-error").removeClass('visible');
                        $('.wrapper').removeClass('modal-show');
                    }, 3000);
                }
            });
        }
    });
});


